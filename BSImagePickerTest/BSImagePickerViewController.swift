//
//  ViewController.swift
//  BSImagePickerTest
//
//  Created by Denis on 19.02.2020.
//  Copyright © 2020 Denis. All rights reserved.
//

import UIKit
import BSImagePicker
import Photos

class BSImagePickerViewController: UIViewController {
    
    @IBOutlet weak var addFotoImageView: UIImageView!
    
    var selectedAssets = [PHAsset]()
    var photoArray = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
    }
    
    
    @IBAction func addImagesClicked(_ sender: Any) {
        let imagePicker = ImagePickerController()
        self.presentImagePicker(imagePicker, animated: true, select: { (asset: PHAsset) in
            
        }, deselect: { (asset: PHAsset) in
            
        }, cancel: { (assets: [PHAsset]) in
            
        }, finish: { (assets: [PHAsset]) in
            for i in 0..<assets.count {
                self.selectedAssets.append(assets[i])
            }
            self.convertAssetToImages()
        }, completion: nil)
    }
    
    func convertAssetToImages() -> Void {
        if selectedAssets.count != 0 {
            for i in 0..<selectedAssets.count {
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                var thumbnail = UIImage()
                option.isSynchronous = true
                manager.requestImage(for: selectedAssets[i],
                                     targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option) { (result, info) in
                                        thumbnail = result!
                }
                let data = thumbnail.jpegData(compressionQuality: 0.75)
                let newImage = UIImage(data: data!)
                self.photoArray.append(newImage! as UIImage)
            }
            self.addFotoImageView.animationImages = self.photoArray
            self.addFotoImageView.animationDuration = 3.0
            self.addFotoImageView.startAnimating()
        }
        print("complete photo array \(self.photoArray)")
    }
    
}




